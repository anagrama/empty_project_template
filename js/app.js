$(document).ready(function() {

//CARGAR MODULOS

  $("header").load("_header.html");
  $("#mb-menu").load("_mobile.html");
  $("footer").load("_footer.html");

// GALERIA DE INSTAGRAM

  if ($('#instagram-component').length > 0) {
    $(function() {
      var accessToken = '6284181.ff93ef3.bbc16c3c41b54ccea88b4489f515a26f';
      $.getJSON('https://api.instagram.com/v1/users/self/media/recent/?access_token=' + accessToken + '&callback=?', function(insta) {
        $.each(insta.data, function(photos, src) {
          if (photos === 4) {
            return false;
          }
          //LAYOUT GALERIA ADENTRO DE .ROW
          $('<div class="col-6 ig-item"><a href="' + src.link + '" class="post" target="_blank">' +
            '<div class="image" style="background-image:url(' + src.images.standard_resolution.url + ');"></div>' +
            '<ul>' +
            '<li class="panio-ig">@PANIOPANADERIA</li>' +
            '<li class="like-count">' + src.likes.count + '</li>' +
            '</ul></a></div>').appendTo('#instagram-component');
        });
      });
    });
  }



  //VALIDACIÓN

  if ($('.page-contact').length > 0) {
    $('.contact-form > input, .contact-form > textarea').blur(function() {
      if (!$(this).val()) {
        $(this).addClass('input-error').val('').attr("placeholder", "Favor de llenar este campo");
        $('.contact-form > form').addClass('form-error');
      }
    });
    $('.contact-form > input, .contact-form > textarea').focus(function() {
      if (!$(this).is('input-error')) {
        $(this).removeClass('input-error').attr("placeholder", "");
      }
    });
//CAMBIAR IMAGEN DE BOTON AL DAR CLICK

    $("body").on("click", "#submit-btn", function() {
      event.preventDefault();

      var el = $(this);

      if (el.text() == el.data("text-swap")) {
        el.text(el.data("text-original"));
        $("#submit-btn").removeClass('sub-sent');

      } else {
        el.data("text-original", el.text());
        el.text(el.data("text-swap"));
        $("#submit-btn").addClass('sub-sent').attr('disabled', true);
        $(".contact-newsletter input").val('Gracias por suscribirte!').attr('disabled', true);
      }
    });
  }

//SLIDER INIT POR SLIDER PRESENTE

  if ($('.home-slider').length > 0) {

    var mySwiper = new Swiper('.home-slider .swiper-container', {
      loop: true,
      direction: 'horizontal',
      effect: 'slide',
      slidesPerView: '1',
      keyboard: {
        enabled: true,
        onlyInViewport: true,
      },
      pagination: {
        el: '.swiper-pagination',
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      autoplay: {
        delay: 4000,
        reverseDirection: true,
      },
    })
  }

  // LOADER

$(window).on('load', function() {
  $('.loader-image').fadeOut(350);
  $('.loader-component').delay(600).fadeOut('slow');
  $('main').css({
    opacity: 1
  });
});

// BOTON MOVIL
$('.mobile-btn').click(function() {
  $(".mobile-btn").toggleClass('open');
  $(".mobile-menu").toggleClass('open');
  $("body").toggleClass('body-no-scroll');
});


// AGREGAR EFECTOS DE ON SCROLL
$(window).on('load', function() {
  $('.element').attr('data-aos', 'fade-in-up');
  AOS.init();
});
